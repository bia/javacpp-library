package plugins.stef.library;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Icy wrapper for the Bytedeco JavaCPP library.
 * 
 * @author Stephane Dallongeville
 */
public class JavaCPPPlugin extends Plugin implements PluginLibrary
{
    //
}
